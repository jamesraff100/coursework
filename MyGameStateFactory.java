package uk.ac.bris.cs.scotlandyard.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import java.util.*;
import javax.annotation.Nonnull;

import com.google.common.collect.Lists;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableGraph;
import com.google.common.graph.ValueGraphBuilder;
import uk.ac.bris.cs.scotlandyard.model.Board.GameState;
import uk.ac.bris.cs.scotlandyard.model.Move.*;
import uk.ac.bris.cs.scotlandyard.model.Piece.*;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYard.*;

/**
 * cw-model
 * Stage 1: Complete this class
 */
public final class MyGameStateFactory implements Factory<GameState> {

	@Nonnull @Override public GameState build(
			GameSetup setup,
			Player mrX,
			ImmutableList<Player> detectives) {

		final class MyGameState implements GameState {

			private MyGameState(final GameSetup setup,
								final ImmutableSet<Piece> remaining,
								final ImmutableList<LogEntry> log,
								final Player mrX,
								final List<Player> detectives) {
				this.setup = setup;
				this.mrX = mrX;
				this.detectives = detectives;
				this.log = log;
				this.remaining = remaining;


				if (this.setup == null) throw new NullPointerException();
				for (Player player : detectives) {
					if (player.piece().isMrX() == true) {
						throw new IllegalArgumentException();
					}
				}
				checkTokens(detectives);
				checkDectectiveTickets(detectives);
				if (this.mrX == null) throw new NullPointerException();
				if (this.detectives == null) throw new NullPointerException();
				if (this.mrX.piece().isMrX() == false) throw new IllegalArgumentException();
				if (setup.moves.isEmpty()) throw new IllegalArgumentException("Moves is empty!");
				checkGraph(setup);
			}


			private GameSetup setup;
			private ImmutableSet<Piece> remaining;
			private ImmutableList<LogEntry> log;
			private Player mrX;
			private List<Player> detectives;
			private ImmutableSet<Move> moves;
			private ImmutableSet<Piece> winner;

			public Player pieceToPlayer(Piece piece) {

				if (piece.isMrX()) {
					return this.mrX;
				}
				else {
					for (Player detective : detectives) {
						if (piece == detective.piece()) {
							return detective;
						}
					}
				}
				return null;
			}


			public void checkGraph(GameSetup setup) {
				ImmutableValueGraph<Integer, ImmutableSet<Transport>> empty = ValueGraphBuilder.undirected()
						.<Integer, ImmutableSet<Transport>>immutable()
						.build();
				if (setup.graph.equals(empty)) {
					throw new IllegalArgumentException();
				}

			}

			public void checkTokens(final List<Player> detectives) {
				List<Piece> tokens = new ArrayList<>();
				List<Integer> locations = new ArrayList<>();
				for (Player player : detectives) {
					for (Piece colour : tokens) {
						if (player.piece() == colour) {
							throw new IllegalArgumentException();
						}
					}
					Integer objectLocation = new Integer(player.location());
					for (Integer location : locations) {
						if (player.location() == location) {
							throw new IllegalArgumentException();
						}
					}
					tokens.add(player.piece());
					locations.add(objectLocation);
				}
			}

			public void checkDectectiveTickets(final List<Player> detectives) {
				for (Player player : detectives) {
					if (player.has(Ticket.SECRET)) {
						throw new IllegalArgumentException();
					}
					if (player.has(Ticket.DOUBLE)) {
						throw new IllegalArgumentException();
					}
				}

			}

			@Override
			public GameSetup getSetup() {
				return this.setup;
			}

			@Override
			public ImmutableSet<Piece> getPlayers() {
				Set<Piece> pieces = new HashSet<>();
				for (Player player : detectives) {
					pieces.add(player.piece());
				}
				pieces.add(mrX.piece());
				ImmutableSet<Piece> result = ImmutableSet.copyOf(pieces);
				return result;
			}

			@Override
			public GameState advance(Move move) {
				return null;
			}

			@Override
			public Optional<Integer> getDetectiveLocation(Detective detective) {
				for (Player player : detectives) {
					if (player.piece() == detective) {
						return Optional.of(player.location());
					}
				}

				return Optional.empty();
			}

			@Override
			public Optional<TicketBoard> getPlayerTickets(Piece piece) {
				//ImmutableMap<Ticket, Integer>() tickets;
				class TicketBoardImp implements TicketBoard {
					TicketBoardImp(Map<Ticket, Integer> tickets) {
						this.tickets = tickets;

					}

					private Map<Ticket, Integer> tickets;


					public int getCount(@Nonnull Ticket ticket) {
						int result = tickets.get(ticket);
						return result;
					}


					public void add(Ticket ticket) {
						Integer number = tickets.get(ticket);
						if(number == 0){
							tickets.put(ticket, 0);
						}
						else {
							tickets.replace(ticket, (number + 1));
						}
					}
					public Map<Ticket, Integer> show(){
						return this.tickets;
					}
					public void whole(Map<Ticket, Integer> map) {
						this.tickets = map;
					}
				}
				Player selected = this.pieceToPlayer(piece);
				for (Piece piece1 : getPlayers()){
					if (piece == piece1 ){
						//Checking if in game
						TicketBoardImp playerBoard = new TicketBoardImp(selected.tickets());
						playerBoard.whole(selected.tickets());
						return Optional.of(playerBoard);
					}
				}
				return Optional.empty();

			}



			@Override
			public ImmutableList<LogEntry> getMrXTravelLog() {
				return this.log;
			}

			@Override
			public ImmutableSet<Piece> getWinner() {
				Set<Piece> pieces = new HashSet<>();
				ImmutableSet result = ImmutableSet.copyOf(pieces);
				return result;
			}

			//Helpers for available moves
			private static Set<SingleMove> makeSingleMoves(GameSetup setup, List<Player> detectives, Player player, int source) {

				// TODO create an empty collection of some sort, say, HashSet, to store all the SingleMove we generate
				Set<SingleMove> moves = new HashSet<>();
				int flag = 0;
				for (int destination : setup.graph.adjacentNodes(source)) {
					for (Player detective : detectives) {
						if (detective.location() == destination) ;
						flag = 1;
					}
					// TODO find out if destination is occupied by a detective
					//  if the location is occupied, don't add to the collection of moves to return
					for (Transport t : setup.graph.edgeValueOrDefault(source, destination, ImmutableSet.of())) {
						Ticket ticket = t.requiredTicket();
						Integer hold = player.tickets().getOrDefault(ticket, 0);
						if (hold != 0 && flag == 1){
							SingleMove move = new SingleMove(player.piece(), source, ticket, destination);
							moves.add(move);
						}
						// TODO find out if the player has the required tickets
						//  if it does, construct a SingleMove and add it the collection of moves to return
					}
					if(player.has(Ticket.SECRET)){
						for (Transport t : setup.graph.edgeValueOrDefault(source, destination, ImmutableSet.of())) {
							SingleMove move = new SingleMove(player.piece(), source, Ticket.SECRET, destination);
							moves.add(move);
						}

					}
					// TODO consider the rules of secret moves here
					//  add moves to the destination via a secret ticket if there are any left with the player
				}

				// TODO return the collection of moves
				return moves;
			}

			private static Set<DoubleMove> makeDoubleMoves(GameSetup setup, List<Player> detectives, Player player, int source){
				Set<SingleMove> moves = new HashSet<>();
				Set<SingleMove> secondNodeMoves = new HashSet<>();
				Set<SingleMove> firstNodeMoves = makeSingleMoves(setup, detectives, player, source);
				Set<Integer> nodes = setup.graph.nodes();
				for (Integer node : nodes){
					secondNodeMoves.addAll(makeSingleMoves(setup, detectives, player, node));
					
				}


				return null;
			}


			@Override
			public ImmutableSet<Move> getAvailableMoves() {
				Set<SingleMove> hold = null;
				for (Piece current : getPlayers()){
					Player player = pieceToPlayer(current);
					hold = makeSingleMoves(getSetup(), detectives, player, player.location());
				}
				ImmutableSet<Move> result = ImmutableSet.copyOf(hold);
				System.out.println(result);


				return result;
			}

		}


		return new MyGameState(setup, ImmutableSet.of(MrX.MRX), ImmutableList.of(), mrX, detectives);
	}

}


